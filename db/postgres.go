package db

import (
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
)

func NewPostgresDB(host, port, user, password, dbname string) (*sqlx.DB, error) {
	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sqlx.Open("postgres", dsn)
	if err != nil {
		return nil, errors.New("error with open sqlx db")
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	log.Println("Successfully connected!")
	return db, nil
}
