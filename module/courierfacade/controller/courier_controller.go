package controller

import (
	"context"
	"encoding/json"
	"github.com/prometheus/client_golang/prometheus"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/citaces/slow-log/module/courierfacade/service"
)

var (
	MoveCourier = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "CourierMove",
	})

	CourierGetStatus = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "CourierGetStatus",
	})
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)

	start := time.Now()
	// получить статус курьера из сервиса courierService используя метод GetStatus
	status := c.courierService.GetStatus(ctx)
	// отправить статус курьера в ответ
	ctx.JSON(http.StatusOK, status)
	since := time.Since(start)
	CourierGetStatus.Observe(float64(since))
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	var err error
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	mData, ok := m.Data.([]byte)
	if !ok {
		log.Fatal(err)
	}
	err = json.Unmarshal(mData, &cm)
	if err != nil {
		log.Fatal(err)
	}
	start := time.Now()
	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
	since := time.Since(start)

	MoveCourier.Observe(float64(since))

}
