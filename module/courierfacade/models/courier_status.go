package models

import (
	cm "gitlab.com/citaces/slow-log/module/courier/models"
	om "gitlab.com/citaces/slow-log/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
