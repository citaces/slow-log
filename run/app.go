package run

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/citaces/slow-log/db"
	"gitlab.com/citaces/slow-log/geo"
	cservice "gitlab.com/citaces/slow-log/module/courier/service"
	cstorage "gitlab.com/citaces/slow-log/module/courier/storage"
	"gitlab.com/citaces/slow-log/module/courierfacade/controller"
	cfservice "gitlab.com/citaces/slow-log/module/courierfacade/service"
	oservice "gitlab.com/citaces/slow-log/module/order/service"
	ostorage "gitlab.com/citaces/slow-log/module/order/storage"
	"gitlab.com/citaces/slow-log/router"
	"gitlab.com/citaces/slow-log/server"
	"gitlab.com/citaces/slow-log/workers/order"
	"log"
	"net/http"
	"os"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {
	dbHost := os.Getenv(`DB_HOST`)
	dbPort := os.Getenv(`DB_PORT`)
	dbUser := os.Getenv(`DB_USER`)
	dbPass := os.Getenv(`DB_PASSWORD`)
	dbName := os.Getenv(`DB_NAME`)
	postgres, err := db.NewPostgresDB(dbHost, dbPort, dbUser, dbPass, dbName)
	if err != nil {
		return err
	}
	log.Println(`successful connect postgres`)

	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}

	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(postgres)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(postgres)
	// инициализация сервиса курьеров
	courierSevice := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierSevice, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	routes.Prometheus(mainRoute)
	prometheus.MustRegister(controller.CourierGetStatus, controller.MoveCourier, ostorage.RepoStatus)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	//serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
